﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class EndVideoNextScene : MonoBehaviour {

	public VideoPlayer vPlayer;

	// Use this for initialization
	void Start () {
		vPlayer.loopPointReached += NextScene;
	}

	private void NextScene(VideoPlayer vP){
		SceneManager.LoadScene (3);
	}
}
