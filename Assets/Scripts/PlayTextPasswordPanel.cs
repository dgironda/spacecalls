﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayTextPasswordPanel : MonoBehaviour {

	private const string SYMBOL_KEY = "textPassword";

	public Text pwField;
	public Text pwLabel;
	public GameObject incorrectLabel;
	public GameObject correctLabel;

	private string textPassword = "";

	// Use this for initialization
	void OnEnable () {

		this.textPassword = this.GetPersistentPassword();

	}


	/// <summary>
	/// Gets the persistent password from PlayerPrefs
	/// </summary>
	/// <returns>The persistent password.</returns>
	private string GetPersistentPassword()
	{
		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey (SYMBOL_KEY)) {
			pwString = PlayerPrefs.GetString (SYMBOL_KEY);
		} else {
			pwString = string.Empty;
		}

		return pwString;
	}



	/// <summary>
	/// Get the text from PasswordInput, save to PlayerPrefs
	/// </summary>
	/// <param name="password">Password.</param>
	public void OnSubmitButtonClicked()
	{
		if (textPassword.ToLower() == pwField.text.ToLower()) {
			this.correctLabel.SetActive(true);

			this.StartCoroutine (WaitTwoSecondsThenComplete ());
		}
		else
		{

			this.incorrectLabel.SetActive(true);

			this.StartCoroutine (WaitTwoSecondsThenResetLabel());
		}

	}


	private IEnumerator WaitTwoSecondsThenResetLabel()
	{
		yield return new WaitForSeconds (2f);

		//Need to disable user interaction during this period.

		this.pwField.text = "";

		this.incorrectLabel.SetActive(false);

	}


	private IEnumerator WaitTwoSecondsThenComplete()
	{
		yield return new WaitForSeconds (2f);

		SceneManager.LoadScene(4);
	}

}
