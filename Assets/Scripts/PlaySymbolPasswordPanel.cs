﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlaySymbolPasswordPanel : MonoBehaviour {


	/// <summary>
	/// used when setting the new password
	/// </summary>
	private int newPasswordIndex; 

	/// <summary>
	/// The length of the password.
	/// </summary>
	private const int PASSWORD_LENGTH = 4;

	/// <summary>
	/// Set PlayerPrefs persistent storage key.
	/// </summary>
	private const string SYMBOL_KEY = "symbolPassword";

	private string[] symbolPassword = new string[]{"0","0","0","0"};

	public TextMeshProUGUI pwLabel;

	private string[] tmpPw = new string[]{"<sprite=12>","<sprite=12>","<sprite=12>","<sprite=12>"};

	public GameObject incorrectLabel;
	public GameObject correctLabel;


	// Use this for initialization
	void OnEnable () {
		// default value
		this.newPasswordIndex = 0;

		//reset tmpPw to blank sprites
		for (int i = 0; i < PASSWORD_LENGTH; i++)
		{
			tmpPw [i] = "<sprite=12>";
		}

		// grab the currently stored password
		this.symbolPassword = this.GetPersistentPassword ();

		//clear pwLabel
		ReplaceOnScreenPassword();

	}

	/// <summary>
	/// Gets the persistent password from PlayerPrefs
	/// </summary>
	/// <returns>The persistent password.</returns>
	private string[] GetPersistentPassword()
	{
		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey (SYMBOL_KEY)) {
			pwString = PlayerPrefs.GetString (SYMBOL_KEY);
		} else {
			pwString = string.Empty;
		}

		// 2. split the string into an array of strings
		string[] splitStrings = pwString.Split(',');

		// safeguard in case the stored password isn't formatted correctly
		if (splitStrings.Length < PASSWORD_LENGTH)
		{
			splitStrings = new string[]{ "0", "0", "0", "0"};
		}
			
		return splitStrings;
	}



	public void OnSymbolButtonClicked(string buttonInput)
	{
		if (newPasswordIndex == 0)
		{

			//reset tmpPw to blank sprites
			for (int i = 0; i < PASSWORD_LENGTH; i++)
			{
				tmpPw [i] = "<sprite=12>";
			}
				
		}

		tmpPw[newPasswordIndex] = buttonInput;

		//Show symbol input on screen
		ReplaceOnScreenPassword ();

		//Check to see if button is correct to the current index position in password
		if (symbolPassword [newPasswordIndex] == buttonInput) {

			//If true, increase the password index
			newPasswordIndex++;

			//When the password index equals the length of the password
			if (newPasswordIndex >= PASSWORD_LENGTH)
			{
				//Reset index
				newPasswordIndex = 0;

				this.pwLabel.fontSize = 130f;
				this.pwLabel.faceColor = new Color32(83, 255, 0, 255);

				this.correctLabel.SetActive(true);

				this.StartCoroutine (WaitTwoSecondsThenComplete());

			}
		}
		else
		{
			//If false, reset password index to 0
			newPasswordIndex = 0;

			this.pwLabel.fontSize = 130f;
			this.pwLabel.faceColor = new Color32(253, 55, 65, 255);

			this.incorrectLabel.SetActive(true);

			this.StartCoroutine (WaitTwoSecondsThenResetLabel());
		}



	}

	private void ReplaceOnScreenPassword()
	{
		string tempString = string.Empty;

		foreach (string s in tmpPw)
		{
			tempString += s;
		}

		this.pwLabel.SetText (tempString);
	}


	private IEnumerator WaitTwoSecondsThenResetLabel()
	{
		yield return new WaitForSeconds (2f);

		//Need to disable user interaction during this period.

		this.pwLabel.fontSize = 200f;
		this.pwLabel.faceColor = new Color32(255, 255, 255, 255);

//		this.pwLabel.SetText ("<sprite=12><sprite=12><sprite=12><sprite=12>");

		this.incorrectLabel.SetActive(false);

		//reset tmpPw to blank sprites
		for (int i = 0; i < PASSWORD_LENGTH; i++)
		{
			tmpPw [i] = "<sprite=12>";
		}


		ReplaceOnScreenPassword();
	}

	private IEnumerator WaitTwoSecondsThenComplete()
	{
		yield return new WaitForSeconds (2f);

		//Need to disable user interaction during this period.

		SceneManager.LoadScene(2);
	}

}
