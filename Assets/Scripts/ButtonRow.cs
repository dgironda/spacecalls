﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonRow : MonoBehaviour
{
	public List<string> LetterList;
	public List<string> SymbolList;
	private bool swapped = false;

	// Use this for initialization
	void Start ()
	{
		this.UpdateButtonText (LetterList);
	}

	public void SetLetterList(List<string> newLetters)
	{
		this.LetterList = newLetters;

		this.UpdateButtonText (LetterList);
	}

	private void UpdateButtonText(List<string> currentList)
	{
		for (int i = 0; i < this.transform.childCount; i++)
		{
			KeyboardButton kbb = this.transform.GetChild(i).GetComponent<KeyboardButton> ();

			kbb.SetText (currentList[i]);
		}
	}

	public void SwapLettersAndSymbols()
	{
		if (!swapped)
		{
			this.UpdateButtonText (this.SymbolList);

			swapped = true;
		}
		else
		{
			this.UpdateButtonText (this.LetterList);

			swapped = false;
		}
	}
}
