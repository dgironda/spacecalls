﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_SetPlayerPreferencePassword : MonoBehaviour {

	private const string SYMBOL_KEY = "symbolPassword";

	// Use this for initialization
	public void DEBUG_RESET () {
		PlayerPrefs.SetString (SYMBOL_KEY, "<sprite=12>,<sprite=12>,<sprite=12>,<sprite=12>");
		Debug.Log (PlayerPrefs.GetString(SYMBOL_KEY));
	}

	public void DEBUG_VIEW (){
		Debug.Log (PlayerPrefs.GetString(SYMBOL_KEY));
	}

}
