﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTextPasswordPanel : MonoBehaviour {

	private const string SYMBOL_KEY = "textPassword";

	public InputField pwField;
	public Text saveLabel;

	// Use this for initialization
	void Start () {

		pwField.text = GetPersistentPassword();
		
	}


	/// <summary>
	/// Gets the persistent password from PlayerPrefs
	/// </summary>
	/// <returns>The persistent password.</returns>
	private string GetPersistentPassword()
	{
		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey (SYMBOL_KEY)) {
			pwString = PlayerPrefs.GetString (SYMBOL_KEY);
		} else {
			pwString = string.Empty;
		}

		return pwString;
	}



	/// <summary>
	/// Get the text from PasswordInput, save to PlayerPrefs
	/// </summary>
	/// <param name="password">Password.</param>
	public void OnSaveButtonClicked()
	{
		PlayerPrefs.SetString(SYMBOL_KEY, pwField.text);

		this.saveLabel.text = " SAVED ";
		this.saveLabel.color = new Color32 (83,255,0,255);

		StartCoroutine (WaitTwoSecondsThenResetLabel());
	}

	private IEnumerator WaitTwoSecondsThenResetLabel()
	{
		yield return new WaitForSeconds (2f);

		this.saveLabel.color = new Color32 (255,255,255,255);

		this.saveLabel.text = "System Override Password";

	}

}
