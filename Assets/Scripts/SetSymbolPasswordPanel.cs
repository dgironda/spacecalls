﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using TMPro;

public class SetSymbolPasswordPanel : MonoBehaviour
{
	/// <summary>
	/// used when setting the new password
	/// </summary>
	private int newPasswordIndex; 

	/// <summary>
	/// The length of the password.
	/// </summary>
	private const int PASSWORD_LENGTH = 4;

	/// <summary>
	/// Set PlayerPrefs persistent storage key.
	/// </summary>
	private const string SYMBOL_KEY = "symbolPassword";

	/// <summary>
	/// the currently stored password on the device
	/// </summary>
	private List<string> currentSymbolPassword;

	public TextMeshProUGUI pwLabel;
		
	// Use this for initialization
	void OnEnable () {
		// default value
		this.newPasswordIndex = 0;

		// grab the currently stored password
		this.currentSymbolPassword = this.GetPersistentPassword ();

		this.ReplaceOnScreenPassword ();
	}


	/// <summary>
	/// Gets the persistent password from PlayerPrefs
	/// </summary>
	/// <returns>The persistent password.</returns>
	private List<string> GetPersistentPassword()
	{
		List<string> storedPassword = new List<string> ();

		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey (SYMBOL_KEY)) {
			pwString = PlayerPrefs.GetString (SYMBOL_KEY);
		} else {
			pwString = string.Empty;
		}

		// 2. split the string into an array of strings
		string[] splitStrings = pwString.Split(',');

		// safeguard in case the stored password isn't formatted correctly
		if (splitStrings.Length < PASSWORD_LENGTH)
		{
			splitStrings = new string[]{ "<sprite=12>", "<sprite=12>", "<sprite=12>", "<sprite=12>"};
		}

		// 3. parse string array values into integers and insert into returned list
		for (int i = 0; i < splitStrings.Length; i++)
		{
			storedPassword.Add (splitStrings[i]);
		}

		return storedPassword;
	}



	public void OnSymbolButtonClicked(string symbolIndex)
	{
		if (newPasswordIndex == 0)
		{
			this.currentSymbolPassword.Clear ();
			for (int i = 0; i < PASSWORD_LENGTH; i++)
			{
				this.currentSymbolPassword.Add ("<sprite=12>");
			}
		}
			
		this.currentSymbolPassword [newPasswordIndex] = symbolIndex;

		ReplaceOnScreenPassword ();

		newPasswordIndex++;

		if (newPasswordIndex >= PASSWORD_LENGTH)
		{
			newPasswordIndex = 0;

			string pwString = string.Join(",", this.currentSymbolPassword.ToArray());
			
			//Save to persistent storage
			PlayerPrefs.SetString(SYMBOL_KEY, pwString);
		}
	}

	private void ReplaceOnScreenPassword()
	{
		string tmpPw = string.Empty;

		foreach (string s in this.currentSymbolPassword)
		{
			tmpPw += s;
		}

		this.pwLabel.SetText (tmpPw);
	}

	/// <summary>
	/// Get the PlayerPrefs symbolPassword and display it on the label
	/// </summary>
	private void ReplaceOnScreenPassword2(){
		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey (SYMBOL_KEY)) {
			pwString = PlayerPrefs.GetString (SYMBOL_KEY);
		} else {
			pwString = string.Empty;
		}

		// Write to pwLabel
		this.pwLabel.SetText (pwString);


	}




}
