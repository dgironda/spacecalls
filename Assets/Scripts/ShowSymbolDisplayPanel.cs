﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowSymbolDisplayPanel : MonoBehaviour {

	public TextMeshProUGUI pwLabel;
	private List<string> currentSymbolPassword;
	private const int PASSWORD_LENGTH = 4;


	// Use this for initialization
	void OnEnable () {
		// grab the currently stored password
		this.currentSymbolPassword = this.GetPersistentPassword ();

		ReplaceOnScreenPassword ();
	}

	/// <summary>
	/// Gets the persistent password from PlayerPrefs
	/// </summary>
	/// <returns>The persistent password.</returns>
	private List<string> GetPersistentPassword()
	{
		List<string> storedPassword = new List<string> ();

		string pwString;

		// 1. grab the string representation of the password from PlayerPrefs
		if (PlayerPrefs.HasKey ("symbolPassword")) {
			pwString = PlayerPrefs.GetString ("symbolPassword");
		} else {
			pwString = string.Empty;
		}

		// 2. split the string into an array of strings
		string[] splitStrings = pwString.Split(',');

		// safeguard in case the stored password isn't formatted correctly
		if (splitStrings.Length < PASSWORD_LENGTH)
		{
			splitStrings = new string[]{ "<sprite=12>", "<sprite=12>", "<sprite=12>", "<sprite=12>"};
		}

		// 3. parse string array values into integers and insert into returned list
		for (int i = 0; i < splitStrings.Length; i++)
		{
			storedPassword.Add (splitStrings[i]);
		}

		return storedPassword;
	}



	private void ReplaceOnScreenPassword()
	{
		string tmpPw = string.Empty;

		foreach (string s in this.currentSymbolPassword)
		{
			tmpPw += s;
		}

		this.pwLabel.SetText (tmpPw);
	}

}
