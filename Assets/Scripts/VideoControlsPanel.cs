﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideoControlsPanel : MonoBehaviour {

	public MediaPlayerCtrl videoScreen;
	public int nextSceneNumber;


	/// <summary>
	/// Grab video element, add three functions for button logic Play/Pause/Stop.
	/// </summary>


	void Start () {
		Debug.Log ("buttslmao");
		this.videoScreen.OnEnd = this.OnVideoEnd;
	}

	private void OnVideoEnd(){
		Debug.Log (nextSceneNumber);
		SceneManager.LoadScene (nextSceneNumber);
	}


	public void StopButton(){
		videoScreen.Stop ();
	}

	public void PauseButton(){
		videoScreen.Pause ();
	}

	public void PlayButton(){
		videoScreen.Play ();
	}


}
