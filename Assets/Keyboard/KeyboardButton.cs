﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyboardButton : MonoBehaviour, IPointerClickHandler
{
	public string TextValue;

	public Text TextField;

	public static event Action<string> OnKeyboardButtonPressed;


	public void Awake()
	{
		
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if(OnKeyboardButtonPressed != null)
		{
			OnKeyboardButtonPressed(this.TextValue);
		}
	}

	public void SetText(string value)
	{
		if(value != null)
		{
			this.TextValue = value;
			this.TextField.text = value;
		}
	}
}
