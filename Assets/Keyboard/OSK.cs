﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OSK : MonoBehaviour
{
	public Text InputTextField;

	public void Start ()
	{
		KeyboardButton.OnKeyboardButtonPressed += this.OnKeyboardButtonPressed;
	}

	public void OnDestroy()
	{
		KeyboardButton.OnKeyboardButtonPressed -= this.OnKeyboardButtonPressed;
	}

	private void OnKeyboardButtonPressed(string buttonText)
	{
		if (this.InputTextField.text.Length < 25)
		{
			if(this.InputTextField != null)
			{
				this.InputTextField.text += buttonText;
			}
		}
	}

	public void OnBackspaceButtonPressed(){
		this.InputTextField.text = this.InputTextField.text.Substring(0, this.InputTextField.text.Length - 1);
	}
}
